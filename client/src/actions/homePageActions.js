/**
 * Created by Rin's on 01/08/2017.
 */

import * as types from '../constants/actionTypes';
import * as businessLogic from '../utils/businessLogic';
import {WORDS_PER_AMOUNT} from '../constants/constants';

export function submit(isDone) {
    return {
        type: types.SUBMIT_FORM,
        isDone: isDone
    }
}

export function onListLevelSelected(index, pageOptions) {
    return {
        type: types.LEVEL_LIST_SELECT,
        levelSelectedIndex: parseInt(index),
        pageOptions: pageOptions
    }
}

export function onListPageSelected(index, amountOptions) {
    return {
        type: types.PAGE_LIST_SELECT,
        pageSelectedIndex: parseInt(index),
        amountOptions: amountOptions
    }
}

export function onListAmountSelected(index, data) {
    return {
        type: types.AMOUNT_LIST_SELECT,
        amountSelectedIndex: parseInt(index),
        data: data
    }
}

function fetchDataRequest() {
    return {
        type: types.FETCH_REQUEST
    }
}

function fetchDataSuccess(data, amountOptions) {
    return {
        type: types.FETCH_SUCCESS,
        data,
        amountOptions
    }
}

function fetchDataError() {
    return {
        type: types.FETCH_ERROR
    }
}

function fetchData(url, method) {
    return fetch(url, {method: method})
        .then(response => Promise.all([response, response.json()]));
}

export function fetchDataWithRedux(level, selectedIndex) {
    return (dispatch) => {
        level = parseInt(level) + 1;
        dispatch(fetchDataRequest());
        console.log(`level: ${level}, page: ${selectedIndex}`);
        const URL = `http://mazii.net/api/jlptkanji/${level}/100/${selectedIndex}`;
        console.log(URL);
        return fetchData(URL, 'GET').then(([response, json]) => {
            if (response.status === 200) {
                console.log(json);
                let data = {
                    results: [],
                    full: []
                };
                if (json.results) {
                    json.results.map((item, index) => {
                        data.full.push({
                            kanji: item.value.kanji,
                            mean: item.value.mean,
                            level: item.value.level,
                            onYomi: item.value.on ? item.value.on.split(" ") : [],
                            kunYomi: item.value.kun ? item.value.kun.split(" ") : [],
                            kotoba: item.value.examples
                        });
                        if (index >= 0 && index < WORDS_PER_AMOUNT) {
                            data.results.push(data.full[index]);
                        }
                    });
                }
                dispatch(fetchDataSuccess(data));
            }
            else {
                dispatch(fetchDataError());
            }
        });
    }
}
