/**
 * Created by Rin's on 09/08/2017.
 */
import * as types from '../constants/actionTypes';

export function changeYomikata(yomikatas) {
    return {
        type: types.CHANGE_YOMIKATA,
        yomikatas: yomikatas
    }
}
