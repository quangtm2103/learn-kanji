/**
 * Created by Rin's on 09/08/2017.
 */
import * as types from '../constants/actionTypes';

export function changeOnYomi(onyomiList) {
    return {
        type: types.CHANGE_ONYOMI,
        onyomiList: onyomiList,
    }
}

export function changeKunYomi(kunyomiList) {
    return {
        type: types.CHANGE_KUNYOMI,
        kunyomiList: kunyomiList,
    }
}
