/**
 * Created by QuangTM2 on 2017/08/09.
 */
import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import KanjiComponent from '../components/KanjiComponent';
import PropTypes from 'prop-types';

class KanjiContainer extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <KanjiComponent
                kanji = {this.props.kanji}
                mean = {this.props.mean}
            />
        );
    }
}

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return {};
}

KanjiContainer.propTypes = {
    kanji: PropTypes.string.isRequired,
    mean: PropTypes.string.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(KanjiContainer);
