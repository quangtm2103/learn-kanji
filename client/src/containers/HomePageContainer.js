/**
 * Created by Rin's on 01/08/2017.
 */
import React from'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import HomePageComponent from '../components/HomePageComponent';
import * as homePageActions from '../actions/homePageActions';
import initialState from '../reducers/initialState';
import {WORDS_PER_AMOUNT, WORD_PER_PAGE} from '../constants/constants';
import * as businessLogic from '../utils/businessLogic';

let that;
class HomePageContainer extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            isDone: null,
            random: 0
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.submitValue = this.submitValue.bind(this);
        this.onListAmountSelected = this.onListAmountSelected.bind(this);
        this.onListLevelSelected = this.onListLevelSelected.bind(this);
        this.onListPageSelected = this.onListPageSelected.bind(this);
        that = this;
    }

    submitValue(isDone) {
        this.setState({isDone: isDone});
        console.log(this.state.isDone);
        if (!this.state.isDone) {
            let randNum = Math.floor(Math.random() * 20);
            this.setState({random: randNum});
            console.log("rand: " + this.state.random);
        }
    }

    onSubmit() {
        let inputData = {
            yomikata : {
                onYomi: initialState.yomiKata.onyomiList,
                kunYomi: initialState.yomiKata.kunyomiList
            },
            kotoba: initialState.kotoba.yomikatas
        };
        let result = true;
        let answer = this.props.data.results[this.state.random];
        console.log(answer);
        answer.onYomi.forEach((item, i) => {
            if (item != inputData.yomikata.onYomi[i]){
                console.log(item + " " + inputData.yomikata.onYomi[i]);
                result = false;
            }
        });
        console.log(result);
        if (answer.kunYomi && answer.kunYomi[0] && answer.kunYomi[0] != inputData.yomikata.kunYomi[0]) {
            result = false;
        }
        console.log(result);
        answer.kotoba.forEach((item, i) => {
            if (item.p != inputData.kotoba[i]){
                console.log(item.p + " " + inputData.kotoba[i]);
                result = false;
            }
        });
        console.log(result);
        that.submitValue(result);
    }

    onListLevelSelected(index) {
        let page = businessLogic.getPageOfLevel(index);
        let pageOptions = [];
        for (let i = 1; i <= page; i ++) {
            pageOptions.push(`Trang ${i}`);
        }
        this.props.homePageActions.onListLevelSelected(index, pageOptions);
        this.props.homePageActions.onListPageSelected(0, this.props.amountOptions);
    }

    onListPageSelected(index) {
        let amountOptions = [];
        this.props.data.full.forEach((item, i) => {
            if (i % WORDS_PER_AMOUNT === 0) {
                amountOptions.push(`Từ ${i + 1} đến ${i + WORDS_PER_AMOUNT}`);
            }
        });
        this.props.homePageActions.onListPageSelected(index, amountOptions);
        this.props.homePageActions.onListAmountSelected(0, this.props.data);
    }

    onListAmountSelected(index) {
        let data = Object.assign({}, this.props.data);
        data.results = [];
        data.full.forEach((item, i) => {
            if (i >= index * WORDS_PER_AMOUNT && i < (index + 1) * WORDS_PER_AMOUNT) {
                data.results.push(item);
            }
        });
        this.props.homePageActions.onListAmountSelected(index, data);
    }

    componentDidMount() {
        console.log(this.props.levelSelectedIndex + ' ' + this.props.pageSelectedIndex);
        this.props.homePageActions.fetchDataWithRedux(0, 0);
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(this.props);
        if (prevProps.pageSelectedIndex != this.props.pageSelectedIndex) {
            this.props.homePageActions.fetchDataWithRedux(this.props.levelSelectedIndex, this.props.pageSelectedIndex);
        }
    }

    render() {
        return (
            <HomePageComponent
                isDone={this.state.isDone}
                usingData={this.props.data.results}
                random={this.state.random}
                onSubmit={this.onSubmit}
                onListAmountSelected={this.onListAmountSelected}
                onListLevelSelected={this.onListLevelSelected}
                onListPageSelected={this.onListPageSelected}
                levelOptions={this.props.levelOptions}
                amountOptions={this.props.amountOptions}
                pageOptions={this.props.pageOptions}
                levelSelectedIndex={this.props.levelSelectedIndex}
                amountSelectedIndex={this.props.amountSelectedIndex}
                pageSelectedIndex={this.props.pageSelectedIndex}
            />
        );
    }
}
function mapStateToProps(state) {
    return {
        isDone: state.homePage.isDone,
        data: state.homePage.data,
        amountSelectedIndex: state.homePage.amountSelectedIndex,
        levelSelectedIndex: state.homePage.levelSelectedIndex,
        pageSelectedIndex: state.homePage.pageSelectedIndex,
        levelOptions: state.homePage.levelOptions,
        amountOptions: state.homePage.amountOptions,
        pageOptions: state.homePage.pageOptions
    };
}
function mapDispatchToProps(dispatch) {
    return {
        homePageActions: bindActionCreators(homePageActions, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(HomePageContainer);
