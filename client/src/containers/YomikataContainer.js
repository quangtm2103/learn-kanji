/**
 * Created by Rin's on 09/08/2017.
 */
import React from'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import YomikataComponent from '../components/YomikataComponent';
import PropTypes from 'prop-types';
import * as yomikataActions from '../actions/yomikataActions';

let that;
class YomikataContainer extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            onyomiResult: [],
            kunyomiResult: []
        };
        this.changeOnYomi = this.changeOnYomi.bind(this);
        this.changeKunYomi = this.changeKunYomi.bind(this);
        that = this;
    }

    changeOnYomi(yomikata, index) {
        this.props.onyomiList[index] = yomikata;
        yomikataActions.changeOnYomi(this.props.onyomiList);
    }

    changeKunYomi(yomikata, index) {
        this.props.kunyomiList[index] = yomikata;
        yomikataActions.changeKunYomi(this.props.kunyomiList);
    }

    render() {
        return (
            <YomikataComponent
                onYomi = {this.props.onYomi}
                kunYomi = {this.props.kunYomi}
                onyomiList = {this.props.onyomiList}
                kunyomiList = {this.props.kunyomiList}
                changeOnYomi = {this.changeOnYomi}
                changeKunYomi = {this.changeKunYomi}
            />
        );
    }
}
function mapStateToProps(state) {
    return {
        onyomiList: state.yomiKata.onyomiList,
        kunyomiList: state.yomiKata.kunyomiList

    };
}
function mapDispatchToProps(dispatch) {
    return {
        yomikataActions: bindActionCreators(yomikataActions, dispatch)
    };
}

YomikataContainer.propTypes = {
    onYomi: PropTypes.array.isRequired,
    kunYomi: PropTypes.array.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(YomikataContainer);
