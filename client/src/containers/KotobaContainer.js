/**
 * Created by Rin's on 09/08/2017.
 */
import React from'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import KotobaComponent from '../components/KotobaComponent';
import PropTypes from 'prop-types';
import * as kotobaActions from '../actions/kotobaActions';

class KotobaContainer extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.changeYomikata = this.changeYomikata.bind(this);
    }

    changeYomikata(yomikata, index) {
        this.props.yomikatas[index] = yomikata;
        kotobaActions.changeYomikata(this.props.yomikatas);
    }

    render() {
        return (
            <KotobaComponent
                kotobaList={this.props.kotobaList}
                yomikatas={this.props.yomikatas}
                changeYomikata={this.changeYomikata}
            />
        );
    }
}
function mapStateToProps(state) {
    return {
        yomikatas: state.kotoba.yomikatas
    };
}
function mapDispatchToProps(dispatch) {
    return {
        kotobaActions: bindActionCreators(kotobaActions, dispatch)
    };
}

KotobaContainer.propTypes = {
    kotobaList: PropTypes.array.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(KotobaContainer);
