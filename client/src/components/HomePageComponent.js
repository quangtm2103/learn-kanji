/**
 * Created by QuangTM on 01/08/2017.
 */
import React from'react';
import KanjiContainer from '../containers/KanjiContainer';
import YomikataContainer from '../containers/YomikataContainer';
import KotobaContainer from '../containers/KotobaContainer';
import DropDownComponent from './DropDownComponent';
import PageAmountComponent from './PageAmountComponent';

class HomePageComponent extends React.Component {
    constructor(props, context) {
        super(props, context);
        console.log(this.props.random);
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
    }

    render() {
        return (
            <div>
                <div className="row main-content">
                    <div className="col-sm-12">
                        <div className="row">
                            <div className="col-sm-6">
                                <DropDownComponent
                                    dropdownTitle={"Level"}
                                    options={this.props.levelOptions}
                                    onListSelect={this.props.onListLevelSelected}
                                    value={this.props.levelSelectedIndex}
                                />
                            </div>
                            <div className="col-sm-6">
                                <PageAmountComponent
                                    pageOptions={this.props.pageOptions}
                                    onListPageSelect={this.props.onListPageSelected}
                                    pageValue={this.props.pageSelectedIndex}
                                    amountOptions={this.props.amountOptions}
                                    onListAmountSelect={this.props.onListAmountSelected}
                                    amountValue={this.props.amountSelectedIndex}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4 col-sm-12">
                        <KanjiContainer
                            kanji={this.props.usingData && this.props.usingData[this.props.random] ? this.props.usingData[this.props.random].kanji : ''}
                            mean={this.props.usingData && this.props.usingData[this.props.random] ? this.props.usingData[this.props.random].mean : ''}
                        />
                        <YomikataContainer
                            onYomi={this.props.usingData && this.props.usingData[this.props.random] ? this.props.usingData[this.props.random].onYomi : []}
                            kunYomi={this.props.usingData && this.props.usingData[this.props.random] ? this.props.usingData[this.props.random].kunYomi : []}
                        />
                    </div>
                    <div className="col-sm-8 col-sm-12">
                        <KotobaContainer
                            kotobaList={this.props.usingData && this.props.usingData[this.props.random] ? this.props.usingData[this.props.random].kotoba : []}
                        />
                        <h1 className="result">{this.props.isDone === true ? "Yeah, Đúng hết rồi!!! <3" : this.props.isDone === false ? "Vẫn sai đó, làm lại đi!" : ""}</h1>
                        <button className="flat-butt flat-primary-butt flat-double-butt flat-primary-double-butt" onClick={this.props.onSubmit}>Submit</button>
                    </div>
                </div>
            </div>
        );
    }
}
export default HomePageComponent
