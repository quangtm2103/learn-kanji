/**
 * Created by Rin's on 09/08/2017.
 */
import React from'react';
import PropTypes from 'prop-types';

class KotobaComponent extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            word: "Từ",
            speech: "Cách đọc",
            mean: "Nghĩa"
        };
    }

    render() {
        return (
            <div className="kotoba-component">
                <div className="row header">
                    <div className="col-sm-3">
                        {this.state.word}
                    </div>
                    <div className="col-sm-4">
                        {this.state.speech}
                    </div>
                    <div className="col-sm-5">
                        {this.state.mean}
                    </div>
                </div>
                {this.props.kotobaList.map((kotoba, index) => {
                    if (kotoba.w && kotoba.p) return (
                        <div className="row kotoba-content" key={index}>
                            <div className="col-sm-3">
                                <div className="row">
                                    <div className="col-xs-4 ">
                                        <span className="kotoba-header">
                                            {this.state.word}
                                        </span>
                                    </div>
                                    <div className="col-sm-12 col-xs-8">
                                        <span className="kotoba">
                                            {kotoba.w}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="row">
                                    <div className="col-xs-4 ">
                                        <span className="kotoba-yomikata-header">
                                            {this.state.speech}
                                        </span>
                                    </div>
                                    <div className="kotoba-yomikata col-sm-12 col-xs-8">
                                        <input className="form-control" type="text" value={this.props.yomikatas[index]}
                                               onChange={(event) => this.props.changeYomikata(event.target.value, index)}/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-5">
                                <div className="row">
                                    <div className="col-xs-4 ">
                                        <span className="kotoba-mean-header">
                                            {this.state.mean}
                                        </span>
                                    </div>
                                    <div className="col-sm-12 col-xs-8">
                                        <span className="kotoba-mean">
                                            {kotoba.m}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
        );
    }
}

KotobaComponent.propTypes = {
    kotobaList: PropTypes.array.isRequired
}

export default KotobaComponent
