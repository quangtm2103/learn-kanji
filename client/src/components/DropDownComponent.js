/**
 * Created by Rin's on 12/08/2017.
 */
import React from'react';
import PropTypes from 'prop-types';

class DropDownComponent extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.onListSelect = this.onListSelect.bind(this);
    }

    onListSelect(value) {
        this.props.onListSelect(value);
    }

    render() {
        return (
            <div className="dropdown-component row">
                <div className="col-xs-5">
                    <label className="dropdown-title pull-right" htmlFor={this.props.dropdownTitle}>
                        {this.props.dropdownTitle}
                    </label>
                </div>
                <div className="col-xs-7">
                    <select name={this.props.dropdownTitle} className="" data-style="" value={this.props.value} onChange={(e) => {
                        this.onListSelect(e.target.value)
                    }}>
                        {this.props.options && this.props.options.map((item, index) => {
                            return (
                                <option value={index} key={index}>
                                    {item}
                                </option>
                            )
                        })}
                    </select>
                </div>
            </div>
        );
    }
}

DropDownComponent.propTypes = {
    options: PropTypes.array,
    onListSelect: PropTypes.func,
    dropdownTitle: PropTypes.string,
    value: PropTypes.number.isRequired
};

export default DropDownComponent
