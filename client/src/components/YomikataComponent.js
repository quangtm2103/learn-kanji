/**
 * Created by Rin's on 09/08/2017.
 */
import React from'react';
import PropTypes from 'prop-types';

class YomikataComponent extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div className="yomikata">
                <div className="onyomi row">
                    <div className="col-sm-4 col-xs-12 title">
                        音
                    </div>
                    <div className="col-sm-8 col-xs-12">
                        <div>
                            <form action="" className="">
                                {this.props.onYomi.map((item, index) => {
                                    if (index === 0) return (
                                        <div className="form-group" key={index}>
                                            <input type="text" className="form-control onyomi-text" value={this.props.onyomiList[index]} onChange={(event) => this.props.changeOnYomi(event.target.value, index)}/>
                                        </div>
                                    );
                                })}
                            </form>
                        </div>
                    </div>
                </div>
                <div className="kunyomi row">
                    <div className="col-sm-4 col-xs-12 title">
                        訓
                    </div>
                    <div className="col-sm-8 col-xs-12">
                        <div>
                            <form action="" className="">
                                {this.props.kunYomi.map((item, index) => {
                                    return (
                                        <div className="form-group" key={index}>
                                            <input type="text" className="form-control kunyomi-text" value={this.props.kunyomiList[index]} onChange={(event) => this.props.changeKunYomi(event.target.value, index)}/>
                                        </div>
                                    );
                                })}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

YomikataComponent.PropTypes = {
    onYomi: PropTypes.array.isRequired,
    kunYomi: PropTypes.array.isRequired
}

export default YomikataComponent
