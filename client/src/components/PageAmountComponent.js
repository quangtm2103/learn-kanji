/**
 * Created by Rin's on 13/08/2017.
 */
import React from'react';
import PropTypes from 'prop-types';

class PageAmountComponent extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.onListPageSelect = this.onListPageSelect.bind(this);
        this.onListAmountSelect = this.onListAmountSelect.bind(this);
    }

    onListPageSelect(value) {
        this.props.onListPageSelect(value);
    }

    onListAmountSelect(value) {
        this.props.onListAmountSelect(value);
    }

    render() {
        return (
            <div className="dropdown-component row">
                <div className="col-xs-5 page-dropdown">
                    <select className="pull-right" data-style="" value={this.props.pageValue} onChange={(e) => {
                        this.onListPageSelect(e.target.value)
                    }}>
                        {this.props.pageOptions && this.props.pageOptions.map((item, index) => {
                            return (
                                <option value={index} key={index}>
                                    {item}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div className="col-xs-7 amount-dropdown">
                    <select className="" data-style="" value={this.props.amountValue} onChange={(e) => {
                        this.onListAmountSelect(e.target.value)
                    }}>
                        {this.props.amountOptions && this.props.amountOptions.map((item, index) => {
                            return (
                                <option value={index} key={index}>
                                    {item}
                                </option>
                            )
                        })}
                    </select>
                </div>
            </div>
        );
    }
}

PageAmountComponent.propTypes = {
    pageOptions: PropTypes.array,
    amountOptions: PropTypes.array,
    onListPageSelect: PropTypes.func,
    onListAmountSelect: PropTypes.func,
    pageValue: PropTypes.number.isRequired,
    amountValue: PropTypes.number.isRequired
};

export default PageAmountComponent
