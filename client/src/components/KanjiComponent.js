import React from 'react';
import PropTypes from 'prop-types';

class KanjiComponent extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.convertJptoHex = this.convertJptoHex.bind(this);
    }

    convertJptoHex(kanji) {
        if (null == kanji || "" == kanji)
            return "";
        if (-1 != kanji.indexOf("「")) {
            kanji = kanji.replace(new RegExp("「", "g"), "");
            kanji = kanji.replace(new RegExp("」", "g"), "");
        }
        kanji = kanji.trim();
        var x = "";
        for (let i = 0; i < kanji.length; i++) {
            x += ("0000" + kanji.charCodeAt(i).toString(16)).substr(-4);
            if (i != kanji.length - 1) {
                x += "_";
            }
        }
        return `http://data.mazii.net/kanji/0${x}.svg`;
    }

    render() {
        return (
            <div className="kanji-component">
                <div className="kanjiImage">
                    <img src={this.convertJptoHex(this.props.kanji)} alt={this.props.mean}/>
                </div>
                <div className="kanjiMean">
                    {this.props.mean}
                </div>
            </div>
        );
    }
}

KanjiComponent.propTypes = {
    kanji: PropTypes.string.isRequired,
    mean: PropTypes.string.isRequired
}

export default KanjiComponent
