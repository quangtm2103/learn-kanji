import React from 'react';
import PropTypes from 'prop-types';
import HomePageContainer from '../containers/HomePageContainer';

// This is a class-based component because the current
// version of hot reloading won't hot reload a stateless
// component at the top-level.
class App extends React.Component {
  render() {
    return (
      <div className="container">
        <HomePageContainer />
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.element
};

export default App;
