import {combineReducers} from 'redux';
import fuelSavings from './fuelSavingsReducer';
import homePageReducer from './homePageReducer';
import kotobaReducer from './kotobaReducer';
import yomikataReducer from './yomikataReducer';
import {routerReducer} from 'react-router-redux';

const rootReducer = combineReducers({
    fuelSavings,
    homePage: homePageReducer,
    kotoba: kotobaReducer,
    yomiKata: yomikataReducer,
    routing: routerReducer
});

export default rootReducer;
