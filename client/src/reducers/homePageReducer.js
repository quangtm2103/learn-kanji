/**
 * Created by Rin's on 01/08/2017.
 */
import * as types from '../constants/actionTypes';
import initialState from './initialState';

export default function homePageReducer(state = initialState.homePage, action) {
    switch (action.type) {
        case types.SUBMIT_FORM:
            return Object.assign({}, state, {
                isDone: action.isDone
            });
        case types.FETCH_REQUEST:
            return state;
        case types.FETCH_SUCCESS:
            console.log(action.data);
            return Object.assign({}, state, {
                data: action.data
            });
        case types.AMOUNT_LIST_SELECT:
            return Object.assign({}, state, {
                amountSelectedIndex: action.amountSelectedIndex,
                data: action.data
            });
        case types.LEVEL_LIST_SELECT:
            console.log(action.pageOptions);
            return Object.assign({}, state, {
                levelSelectedIndex: action.levelSelectedIndex,
                pageOptions: action.pageOptions
            });
        case types.PAGE_LIST_SELECT:
            return Object.assign({}, state, {
                pageSelectedIndex: action.pageSelectedIndex,
                amountOptions: action.amountOptions
            });
        default:
            return state;
    }
}
