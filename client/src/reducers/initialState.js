export default {
    fuelSavings: {
        newMpg: '',
        tradeMpg: '',
        newPpg: '',
        tradePpg: '',
        milesDriven: '',
        milesDrivenTimeframe: 'week',
        displayResults: false,
        dateModified: null,
        necessaryDataIsProvidedToCalculateSavings: false,
        savings: {
            monthly: 0,
            annual: 0,
            threeYear: 0
        }
    },
    homePage: {
        isDone: false,
        data: {
            results: [],
            full: []
        },
        amountSelectedIndex: 0,
        levelSelectedIndex: 0,
        pageSelectedIndex: 0,
        levelOptions: ['N1', 'N2', 'N3', 'N4', 'N5'],
        pageOptions: [
            'Trang 1',
            'Trang 2',
            'Trang 3',
            'Trang 4',
            'Trang 5',
            'Trang 6',
            'Trang 7',
            'Trang 8',
            'Trang 9',
            'Trang 10',
            'Trang 11',
            'Trang 12',
            'Trang 13'
        ],
        amountOptions: [
            'Từ 1 đến 20',
            'Từ 21 đến 40',
            'Từ 41 đến 60',
            'Từ 61 đến 80',
            'Từ 81 đến 100'
        ]
    },
    kanji: {
        kanji: '',
        mean: ''
    },
    yomiKata: {
        onyomiList: [],
        kunyomiList: []
    },
    kotoba: {
        yomikatas: []
    }
};
