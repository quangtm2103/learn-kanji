/**
 * Created by Rin's on 10/08/2017.
 */
import * as types from '../constants/actionTypes';
import initialState from './initialState';

export default function yomikataReducer(state = initialState.yomiKata, action) {
    switch (action.type) {
        case types.CHANGE_ONYOMI:
            return Object.assign({}, state, {
                onyomiList: action.onyomiList
            });
        case types.CHANGE_KUNYOMI:
            return Object.assign({}, state, {
                kunyomiList: action.kunyomiList
            });
        default:
            return state;
    }
}
