/**
 * Created by Rin's on 09/08/2017.
 */
import * as types from '../constants/actionTypes';
import initialState from './initialState';

export default function kotobaReducer(state = initialState.kotoba, action) {
    switch (action.type) {
        case types.CHANGE_YOMIKATA:
            console.log(action.yomikatas);
            return Object.assign({}, state, {
                yomikatas: action.yomikatas
            });
        default:
            return state;
    }
}
