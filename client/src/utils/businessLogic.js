/**
 * Created by Rin's on 13/08/2017.
 */
export function convertIndexToPage(index) {
    return Math.floor(index / 5);
}

export function getPageOfLevel(level) {
    switch (level) {
        case 1:
            return 13;
        case 2:
            return 4;
        case 3:
            return 4;
        case 4:
            return 2;
        case 5:
            return 1;
    }
}
